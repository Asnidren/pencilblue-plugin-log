# PencilBlue Plugin Log

This projet contains sources of Asnidren's PencilBlue Plugin Log.


## Build

No need.

## Install

In PencilBlue, in /plugins/ folder, create a folder named "asnidren-plugin-importexport". Copy in it the files that are into /src/ folder. Run PencilBlue, and install the plugin as usual.

## Versions

0.0.1 - is for PencilBlue 0.7.

## History

0.0.1 - First version, for PencilBlue 0.7. 

### Tech

* [fileReader.js] for angular 1.5.

[fileReader.js] : <http://odetocode.com/blogs/scott/archive/2013/07/03/building-a-filereader-service-for-angularjs-the-service.aspx>


## Issues

