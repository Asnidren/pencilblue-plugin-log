
//dependencies
var path = require('path');
var fs = require('fs');
var readline = require('readline');

module.exports = function LogViewerController(pb) {

  //PB dependencies
  var util           = pb.util;
  var PluginService  = pb.PluginService;
  var TopMenuService = pb.TopMenuService;
  var BaseController = pb.BaseController;
  var MediaService = pb.MediaService;


  var URI = '/admin/plugins/log';
  var ID = 'log_viewer';
  var TEMPLATE = '/admin/logviewer';


  // Instantiate the controller & inherit from the PencilBlue base controller
  function LogViewerController(){};
  util.inherits(LogViewerController, BaseController);

  // The render function is called by the router to render the page.
  LogViewerController.prototype.render = function(cb) {
  	var self = this;

    var contents = undefined;
    //pb.log.debug("LogViewerController.render...");
    
    /*var rd = readline.createInterface({
        input: fs.createReadStream(pb.config.logging.file),
        output: process.stdout,
        terminal: false
    });

    var contents = [];
    rd.on('line', function(line) {
        //pb.log.debug(line);
        var lineparsed = JSON.parse(line);
        //pb.log.debug(lineparsed);
        contents.push(lineparsed);
    });
    rd.on('close', function(){
      //pb.log.debug(contents);
      self.doRender(contents,cb);
    });*/
    self.doRender(contents,cb);
  };

  LogViewerController.prototype.doRender = function(contents,cb){
    var self = this;
    
    var navLevel = ['log',ID];
    var objects = {
        navigation: pb.AdminNavigation.get(self.session, navLevel, self.ls),
        currentUserId: self.session.authentication.user_id,
        contents: contents
    };

    self.setPageName('Log viewer');
    self.ts.registerLocal('angular_script', '');
    var angularObjects = new pb.TemplateValue(pb.ClientJs.getAngularObjects(objects), false);
    //console.log(angularObjects);
    self.ts.registerLocal('angular_objects', angularObjects);
    var template = TEMPLATE;
    self.ts.load(template, function(loadErr, result) {
      if (loadErr) {
          pb.log.error(loadErr);
          cb(loadErr, null);
      }
      cb({ content: result });
    });
  };


  LogViewerController.getRoutes = function(cb) {
    var routes = [
        {
            method: 'get',
            path: URI,
            auth_required: true,
            access_level: pb.SecurityService.ACCESS_ADMINISTRATOR,
            content_type: 'text/html'
        }
    ];
    cb(null, routes);
  };




  return LogViewerController;
};