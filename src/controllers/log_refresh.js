
//dependencies
var path = require('path');
var fs = require('fs');
var readline = require('readline');

module.exports = function LogRefreshController(pb) {

  //PB dependencies
  var util           = pb.util;
  var PluginService  = pb.PluginService;
  var TopMenuService = pb.TopMenuService;
  var BaseController = pb.BaseController;
  var MediaService = pb.MediaService;


  var URI = '/admin/plugins/log/refresh';
  var ID = 'log_refresh';
  var TEMPLATE = '/admin/json';


  // Instantiate the controller & inherit from the PencilBlue base controller
  function LogRefreshController(){};
  util.inherits(LogRefreshController, BaseController);

  // The render function is called by the router to render the page.
  LogRefreshController.prototype.render = function(cb) {
  	var self = this;

    var contents = '';
    
    var rd = readline.createInterface({
        input: fs.createReadStream(pb.config.logging.file),
        output: process.stdout,
        terminal: false
    });

    var contents = [];
    rd.on('line', function(line) {
        //pb.log.debug(line);
        var lineparsed = JSON.parse(line);
        //pb.log.debug(lineparsed);
        contents.push(lineparsed);
    });
    rd.on('close', function(){
      //pb.log.debug(contents);
      self.doRender(contents,cb);
    });
  };

  LogRefreshController.prototype.doRender = function(contents,cb){
    var self = this;
    
    //pb.log.debug("LogRefreshController.doRender : ", result);
    var result = JSON.stringify(contents);
    //pb.log.debug("LogRefreshController.doRender : ", result);
    var value = new pb.TemplateValue(result,false);
    //pb.log.debug("LogRefreshController.doRender : ", value);
    self.ts.registerLocal('angular_objects', value);
    var template = TEMPLATE;
    var output = {
        headers: {
              'Access-Control-Allow-Origin': '*',
              'Access-Control-Allow-Headers':  'Origin, X-Requested-With, Content-Type, Accept',
              'Access-Control-Allow-Methods': 'POST'/*,
              'content-type': 'application/json'*/
        }            
    };
    self.ts.load(template, function(loadErr, result) {
        output.content = result;
        cb(output);
    });
  };


  LogRefreshController.getRoutes = function(cb) {
    var routes = [
        {
            method: 'post',
            path: URI,
            auth_required: true,
            access_level: pb.SecurityService.ACCESS_ADMINISTRATOR,
            content_type: 'text/html'
        }
    ];
    cb(null, routes);
  };




  return LogRefreshController;
};