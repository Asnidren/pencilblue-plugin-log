module.exports = function AsnidrenPluginLog(pb) {

	/**
   * ImportExport - A PencilBlue plugin to manage import export.
   *
   * @author Sandrine Prousteau <sandrine.prousteau@gmail.com>
   */
  function LogViewer(){}



  // new admin menu entries -------------------
  var LOG_ADMIN_NAV = Object.freeze({
      id: "log",
      name: 'log',
      title: "Log",
      icon: "wrench",
      href: "#",
      access: pb.SecurityService.ACCESS_ADMINISTRATOR,
      children: [
      	{
          divider: true,
          id: 'log_viewer',
          name: 'log_viewer',
          title: 'Log viewer',
          icon: 'wrench',
          href: '/admin/plugins/log'
        }
      ]
  });



















  /**
   * Called when the application is being installed for the first time.
   * @static
   * @method onInstallWithContext
   * @param cb A callback that must be called upon completion.  cb(Error, Boolean).
   * The result should be TRUE on success and FALSE on failure
   */
  LogViewer.onInstallWithContext = function(context, cb) {
      pb.log.debug('LogViewer : onInstallWithContext');
      try{
        // Add new admin menu entries
        var site = pb.SiteService.getCurrentSite(context.site);
        if(pb.AdminNavigation.addToSite(LOG_ADMIN_NAV,site)){
          pb.log.info('LogViewer [onInstallWithContext] : new admin menu entries added');
        }else{
          pb.log.info('LogViewer [onInstallWithContext] : new admin menu entries already added');
        }
      }catch(err){
        pb.log.error('LogViewer : Error during onInstall(), adding admin menu entries : ', err);
      }finally{
        cb(null, true);
      }
  };



  /**
   * Called when the application is being installed for the first time.
   *
   * @param cb A callback that must be called upon completion.  cb(err, result).
   * The result is ignored
   */
  LogViewer.onInstall = function(cb) {
      pb.log.debug('LogViewer : onInstall');
  		try{
  			// Add new admin menu entries
        if(pb.AdminNavigation.addToSite(LOG_ADMIN_NAV)){
          pb.log.info('LogViewer [onInstall] : new admin menu entries added');
        }else{
          pb.log.info('LogViewer [onInstall] : new admin menu entries already added');
        }
  		}catch(err){
  			pb.log.error('LogViewer : Error during onInstall(), adding admin menu entries : ', err);
  		}finally{
  			cb(null, true);
  		}
  };


  /**
   * Called when the application is uninstalling this plugin.  The plugin should
   * make every effort to clean up any plugin-specific DB items or any in function
   * overrides it makes.
   *
   * @param context
   * @param cb A callback that must be called upon completion.  cb(Error, Boolean).
   * The result should be TRUE on success and FALSE on failure
   */
  LogViewer.onUninstallWithContext = function (context, cb) {
      pb.log.debug('LogViewer : onUninstallWithContext');
      try{
        var site = pb.SiteService.getCurrentSite(context.site);
        if(pb.AdminNavigation.removeFromSite(LOG_ADMIN_NAV,site)){
          pb.log.info('LogViewer [onUninstallWithContext] : new admin menu entries removed');
        }else{
          pb.log.info('LogViewer [onUninstallWithContext] : new admin menu entries already removed');
        }
      }catch(err){
          pb.log.error("LogViewer : " + 'Error during onUninstallWithContext() : ' + err);
      }
  };

  /**
   * Called when the application is uninstalling this plugin.  The plugin should
   * make every effort to clean up any plugin-specific DB items or any in function
   * overrides it makes.
   *
   * @param cb A callback that must be called upon completion.  cb(err, result).
   * The result is ignored
   */
  LogViewer.onUninstall = function(cb) {
      pb.log.debug('LogViewer : onUninstall');
      try{
  			// Remove new admin menu entries
        if(pb.AdminNavigation.removeFromSite(LOG_ADMIN_NAV)){
          pb.log.info('LogViewer [onUninstall] : new admin menu entries removed');
        }else{
          pb.log.info('LogViewer [onUninstall] : new admin menu entries not removed');
        }
  		}catch(err){
  			pb.log.error('LogViewer : Error during onUninstall(), adding admin menu entries : ', err);
  		}finally{
  			cb(null, true);
  		}
  };

  /**
   * Called when the application is starting up. The function is also called at
   * the end of a successful install. It is guaranteed that all core PB services
   * will be available including access to the core DB.
   *
   * @param cb A callback that must be called upon completion.  cb(err, result).
   * The result is ignored
   */
  LogViewer.onStartup = function(cb) {
      pb.log.debug('LogViewer : onStartup');
      try{
        // Add new admin menu entries
        if(pb.AdminNavigation.addToSite(LOG_ADMIN_NAV)){
          pb.log.info('LogViewer [onStartup] : new admin menu entries added');
        }else{
          pb.log.info('LogViewer [onStartup] : new admin menu entries already added');
        }
      }catch(err){
        pb.log.error('LogViewer : Error during onStartup(), adding admin menu entries');
        pb.log.error(err);
      }finally{
        cb(null, true);
      }
  };

  /**
   * Called when the application is gracefully shutting down.  No guarantees are
   * provided for how much time will be provided the plugin to shut down.
   *
   * @param cb A callback that must be called upon completion.  cb(err, result).
   * The result is ignored
   */
  LogViewer.onShutdown = function(cb) {
      pb.log.debug('LogViewer : onShutdown');
      try{
        // Remove new admin menu entries
        if(pb.AdminNavigation.removeFromSite(LOG_ADMIN_NAV)){
          pb.log.info('LogViewer [onShutdown] : new admin menu entries removed');
        }else{
          pb.log.info('LogViewer [onShutdown] : new admin menu entries not removed');
        }
      }catch(err){
        pb.log.error('LogViewer : Error during onShutdown(), adding admin menu entries : ', err);
      }finally{
        cb(null, true);
      }
  };

  //exports
  return LogViewer;

}